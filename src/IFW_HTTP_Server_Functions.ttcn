///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//  File:               IFW_HTTP_Server_Functions.ttcn
//  Description:
//  Rev:                R1B
//  Prodnr:             CNL 113 910
//  Updated:            2021-02-03
//  Contact:            http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////
module IFW_HTTP_Server_Functions
{
  import from IFW_HTTP_Server_Definitions all;
  import from HTTPmsg_Types all;
  import from IPL4asp_Types all;
  import from IPL4asp_PortType all;
  import from IFW_Common all;
  import from TCCMessageHandling_Functions all;
  import from HTTPmsg_MessageLen all;

  function f_HTTP_Server_init() runs on IFW_HTTP_Server_CT
  {
    log(%definitionId, " started");
    var Result vl_result;

    log("Mapping started");
    map(self:IPL4_PCO,system:IPL4_PCO);
    var f_IPL4_getMsgLen vl_f := refers(f_HTTP_Server_getMessageLength);
    f_IPL4_setGetMsgLen(IPL4_PCO, -1, vl_f, {});

    log("Creating the server socket");
    vl_result := f_IPL4_listen(IPL4_PCO, ctx.localHost, ctx.localPort, {tcp := {}}, {{reuseAddress := {enable := true}}});
    f_checkResult(vl_result);
    ctx.serverConnId := vl_result.connId;

    log(%definitionId, " finished");
  }	

  function f_HTTP_Server_cleanUp() runs on IFW_HTTP_Server_CT
  {
    log(%definitionId, " started");
    var Result vl_result;

    log("Closing connection");
    if (ctx.serverConnId >= 0)
    {
      vl_result := f_IPL4_close(IPL4_PCO, ctx.serverConnId, {tcp := {}});
      //f_checkResult(vl_result);		  
    }

    log(%definitionId, " finished");
  }
	
  function f_HTTP_Server_getContext() runs on IFW_HTTP_Server_CT
  return HttpServerContext
  {
    return ctx;
  }
	
  function f_HTTP_Server_setContext(in HttpServerContext p_ctx) runs on IFW_HTTP_Server_CT
  {
    ctx := p_ctx;
  }
	
  function f_HTTP_Server_setMessageToSend(in HTTPMessage p_msg) runs on IFW_HTTP_Server_CT
  {
    msgToSend := p_msg;
  }	
	
  function f_HTTP_Server_send() runs on IFW_HTTP_Server_CT
  {
    var octetstring v_encoded;

    v_encoded := enc_HTTPMessage(msgToSend);

    var ASP_SendTo vl_send;

    vl_send.connId := ctx.clientConnId;
    vl_send.remName := ctx.remoteHost;
    vl_send.remPort := ctx.remotePort;
    vl_send.proto := {tcp := {}}

    vl_send.msg := v_encoded;

    log("HTTP PDU: ", msgToSend);
    log("IPL4 PDU: ", vl_send);
    IPL4_PCO.send(vl_send);
  }
	
  function f_HTTP_Server_receive(in float p_maxWait := 10.0) runs on IFW_HTTP_Server_CT
  {
    timer t_Timeout := p_maxWait;
    if (p_maxWait > 0.0) { t_Timeout.start; }

    var ASP_RecvFrom v_ipl4Recv;
    var ASP_Event v_ipl4Event;

    alt
    {
      [] IPL4_PCO.receive(ASP_RecvFrom:?) -> value v_ipl4Recv
      {
        log("Received: ", v_ipl4Recv);

        if (ispresent(v_ipl4Recv.connId)) { ctx.clientConnId := v_ipl4Recv.connId; }

	var HTTPMessage v_httpMsg;
	dec_HTTPMessage(v_ipl4Recv.msg, v_httpMsg);

        lastReceived := v_httpMsg;
        ctx.remoteHost := v_ipl4Recv.remName;
        ctx.remotePort := v_ipl4Recv.remPort;
        log("IPL4 PDU: ", lastReceived);
        log("HTTP PDU: ", v_httpMsg);
      }
      [] IPL4_PCO.receive(ASP_Event:?) -> value v_ipl4Event
      {
	log("Received: ", v_ipl4Event);
	repeat;
      }
      [] t_Timeout.timeout
      {
	lastReceived := c_HTTPMessage_empty;
      }
    }
  }
  
  function f_HTTP_Server_check(in template HTTPMessage p_expected) runs on IFW_HTTP_Server_CT
  return ReturnBoolean
  {
    if (not match(lastReceived, p_expected)) 
    {
      log("CHECK: last received is not matching with expected template");
      return false;
    }

    log("CHECK: return true");
    return true;
  }	

  function f_HTTP_Server_close() runs on IFW_HTTP_Server_CT
  {
    log(%definitionId, " started");
    var Result vl_result;

    log("Closing connection");
    if (ctx.serverConnId >= 0)
    {
      vl_result := f_IPL4_close(IPL4_PCO, ctx.serverConnId, {tcp := {}});
      //f_checkResult(vl_result);		  
    }

    log(%definitionId, " finished");
  }
	
  function f_HTTP_Server_getMessageLength(in octetstring stream, inout ro_integer args)
  return integer
  {
    // return f_TCCMessageHandling_getMessageLength(stream)
    return f_HTTPMessage_len(stream);
  }
    
  function f_HTTP_lookupHeader(charstring p_headerName, in HeaderLines p_headers)
  return integer
  {
    for (var integer i:=0; i<sizeof(p_headers); i:=i+1)
    {
      if (p_headers[i].header_name == p_headerName) { return i; }
    }
    return -1;
  }
}
