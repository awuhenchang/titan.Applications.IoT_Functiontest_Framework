///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//  File:               MqttTestSuite_Interop.ttcn
//  Description:
//  Rev:                R1B
//  Prodnr:             CNL 113 910
//  Updated:            2021-02-03
//  Contact:            http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////
module MqttTestSuite_Interop
{
  import from IoT_FT_Framework_Definitions all;
  import from IoT_FT_Framework_Functions all;
  import from IFW_MQTT_Client_TestSteps all;
  import from MQTT_v3_1_1_Types all;

  testcase tc_Mqtt_client_connect_disconnect() runs on IFW_MAIN_CT
  {
    setverdict(pass);
	 
	var integer mqtt_client := f_IFW_addComponent(MQTT_CLIENT, "mqtt_client");
	
	f_IFW_MqttClient_setRemote(mqtt_client, "mqtt_server");
	  
	f_IFW_initComponents();
	  
	f_connect(mqtt_client);
	
	f_disconnect(mqtt_client);    
    
    f_IFW_cleanUp();
  }
  
  testcase tc_Mqtt_client_subscribe() runs on IFW_MAIN_CT
  {
    setverdict(pass);
	 
	var integer mqtt_client := f_IFW_addComponent(MQTT_CLIENT, "mqtt_client");
	
	f_IFW_MqttClient_setRemote(mqtt_client, "mqtt_server");
	  
	f_IFW_initComponents();
	  
	f_connect(mqtt_client);
	
    f_IFW_MqttClient_setMessageToSend(mqtt_client, valueof(t_subscribe("#", 2, AT_MOST_ONCE_DELIVERY)));
	f_IFW_MqttClient_send(mqtt_client);
	
    f_IFW_MqttClient_receive(mqtt_client);
    f_handleVerdict(f_IFW_MqttClient_check(mqtt_client, tr_suback({0}, 2)));
	
	f_disconnect(mqtt_client);    
    
    f_IFW_cleanUp();
  }
  
  testcase tc_Mqtt_client_unsubscribe() runs on IFW_MAIN_CT
  {
    setverdict(pass);
	 
	var integer mqtt_client := f_IFW_addComponent(MQTT_CLIENT, "mqtt_client");
	var integer vl_id := 2;
	
	f_IFW_MqttClient_setRemote(mqtt_client, "mqtt_server");
	  
	f_IFW_initComponents();
	  
	f_connect(mqtt_client);
	
	f_subscribe(mqtt_client, "room/topic", vl_id, AT_MOST_ONCE_DELIVERY);
	
	// Unsubscribe
    f_IFW_MqttClient_setMessageToSend(mqtt_client, valueof(t_unsubscribe("room/topic", 3)));
	f_IFW_MqttClient_send(mqtt_client);
	
    f_IFW_MqttClient_receive(mqtt_client);
    f_handleVerdict(f_IFW_MqttClient_check(mqtt_client, tr_unsuback(3)));   
	
	f_disconnect(mqtt_client);    
    
    f_IFW_cleanUp();
  }
  
  testcase tc_Mqtt_client_publish_qos0() runs on IFW_MAIN_CT
  {
    setverdict(pass);
	 
	var integer mqtt_client_A := f_IFW_addComponent(MQTT_CLIENT, "mqtt_client");
	var integer mqtt_client_B := f_IFW_addComponent(MQTT_CLIENT, "mqtt_client2");
	
	var charstring vl_topic := "room/topic";
	var integer vl_id := 1;
	
	f_IFW_MqttClient_setRemote(mqtt_client_A, "mqtt_server");
	f_IFW_MqttClient_setRemote(mqtt_client_B, "mqtt_server");
	  
	f_IFW_initComponents();
	  
	f_connect(mqtt_client_A);
	f_connect(mqtt_client_B);
	
	f_subscribe(mqtt_client_B, vl_topic, vl_id, AT_MOST_ONCE_DELIVERY);
	
    // Publish - QoS 0
    f_IFW_MqttClient_setMessageToSend(mqtt_client_A, 
      valueof(t_publish(vl_topic, omit, AT_MOST_ONCE_DELIVERY))
    );
	f_IFW_MqttClient_send(mqtt_client_A);
	
    f_IFW_MqttClient_receive(mqtt_client_B);
    f_handleVerdict(f_IFW_MqttClient_check(mqtt_client_B, 
    	t_publish(vl_topic, omit, AT_MOST_ONCE_DELIVERY))
    );   
	
	f_disconnect(mqtt_client_A);
	f_disconnect(mqtt_client_B);    
    
    f_IFW_cleanUp();
  }
  
  testcase tc_Mqtt_client_publish_qos1() runs on IFW_MAIN_CT
  {
    setverdict(pass);
	 
	var integer mqtt_client_A := f_IFW_addComponent(MQTT_CLIENT, "mqtt_client");
	var integer mqtt_client_B := f_IFW_addComponent(MQTT_CLIENT, "mqtt_client2");
	
	var charstring vl_topic := "room/topic";
	var integer vl_id := 1;
	
	f_IFW_MqttClient_setRemote(mqtt_client_A, "mqtt_server");
	f_IFW_MqttClient_setRemote(mqtt_client_B, "mqtt_server");
	  
	f_IFW_initComponents();
	  
	f_connect(mqtt_client_A);
	f_connect(mqtt_client_B);
	
	f_subscribe(mqtt_client_B, vl_topic, vl_id, AT_MOST_ONCE_DELIVERY);
	
    // Publish - QoS 0
    f_IFW_MqttClient_setMessageToSend(mqtt_client_A, 
      valueof(t_publish(vl_topic, vl_id, AT_LEAST_ONCE_DELIVERY))
    );
	f_IFW_MqttClient_send(mqtt_client_A);
	
    f_IFW_MqttClient_receive(mqtt_client_A);
    f_handleVerdict(f_IFW_MqttClient_check(mqtt_client_A, 
    	t_puback(vl_id))
    );
	
    f_IFW_MqttClient_receive(mqtt_client_B);
    f_handleVerdict(f_IFW_MqttClient_check(mqtt_client_B, 
    	t_publish(vl_topic, *, AT_MOST_ONCE_DELIVERY))
    );
	
	f_disconnect(mqtt_client_A);
	f_disconnect(mqtt_client_B);    
    
    f_IFW_cleanUp();
  }  
  
  template MQTT_v3_1_1_ReqResp t_publish(
    in charstring p_topic,
    in template integer p_id,
    in QoS p_qos) := 
  {
    publish := {
    	header := {
    		dup_flag := '0'B,
    		qos_level := p_qos,
    		retain_flag := '0'B
    	},
    	topic_name := p_topic,
    	packet_identifier := p_id,
    	payload := char2oct("publish qos 0 payload")
    }
  }
  
  template MQTT_v3_1_1_ReqResp t_puback(
    in template integer p_id) := 
  {
    puback := {
    	header := { flags := '0000'B },
    	packet_identifier := p_id
    }
  }
  
  function f_subscribe(
  	in integer p_client,
  	in charstring p_topic,
  	inout integer p_id,
  	in QoS p_qos)
  runs on IFW_MAIN_CT
  {
    f_IFW_MqttClient_setMessageToSend(p_client, valueof(t_subscribe(p_topic, p_id, p_qos)));
	f_IFW_MqttClient_send(p_client);
	
    f_IFW_MqttClient_receive(p_client);
    f_handleVerdict(f_IFW_MqttClient_check(p_client, tr_suback({?}, p_id)));
    
    f_nextPacketId(p_id);
  }
  
  template MQTT_v3_1_1_ReqResp t_subscribe(
  	in charstring p_topic,
  	in integer p_id,
  	in QoS p_qos) := 
  {
    subscribe := {
      header := { flags := '0010'B },
      packet_identifier := p_id,
      payload := {
        {
    	  topic_filter := p_topic,
    	  requested_qos := p_qos
        }
      }
    }
  }
  
  template MQTT_v3_1_1_ReqResp tr_suback(in template IntegerList p_code, in template integer p_id) := 
  {
    suback := {
      header := { flags := '0000'B },
      packet_identifier := p_id,
      payload := {
		return_code := p_code
      }
    }
  }
  
  function f_unsubscribe(in integer p_client, in charstring p_topic, inout integer p_id)
  runs on IFW_MAIN_CT
  {
    f_IFW_MqttClient_setMessageToSend(p_client, valueof(t_unsubscribe(p_topic, p_id)));
	f_IFW_MqttClient_send(p_client);
	
    f_IFW_MqttClient_receive(p_client);
    f_handleVerdict(f_IFW_MqttClient_check(p_client, tr_unsuback(p_id)));
    
    f_nextPacketId(p_id);
  }
  
  template MQTT_v3_1_1_ReqResp t_unsubscribe(in charstring p_topic, in integer p_id) := 
  {
    unsubscribe := {
      header := { flags := '0010'B },
      packet_identifier := p_id,
      payload := { topic_filter := { p_topic } }
      //{{
      //	  topic_filter := p_topic
      //}}
    }
  }
  
  template MQTT_v3_1_1_ReqResp tr_unsuback(in template integer p_id) := 
  {
    unsuback := {
      header := { flags := '0000'B },
      packet_identifier := p_id
    }
  }
  
  function f_connect(in integer p_client) runs on IFW_MAIN_CT
  {
	f_IFW_MqttClient_setMessageToSend(p_client, valueof(t_connect(p_client)));
	f_IFW_MqttClient_send(p_client);
	
    f_IFW_MqttClient_receive(p_client);
    f_handleVerdict(f_IFW_MqttClient_check(p_client, tr_connectAck));
  }
  
  function f_disconnect(in integer p_client) runs on IFW_MAIN_CT
  {
  	f_IFW_MqttClient_setMessageToSend(p_client, valueof(t_disconnect));
	f_IFW_MqttClient_send(p_client);
  }
  
  template MQTT_v3_1_1_ReqResp t_connect(integer p_id) := 
  {
    connect_msg := {
      header := { flags := '0000'B },
      name := "MQTT",
      protocol_level := 4,
      flags := {
        user_name_flag := '0'B,
    	password_flag := '0'B,
    	will_retain := '0'B,
    	will_qos := AT_MOST_ONCE_DELIVERY,
    	will_flag := '0'B,
    	clean_session := '1'B
      },
      keep_alive := 0,
      payload := {        
        client_identifier := "",          
        //{
    	//  stringLength := 0,
    	//  stringItem := "myclientid_"&int2str(p_id)
    	//},
    	will_topic := omit,
    	will_message := omit,
    	user_name := omit,
    	password := omit
      }
    }
  }
  
  template MQTT_v3_1_1_ReqResp tr_connectAck := 
  {
    connack := {
      header := { flags := '0000'B },
      session_present_flag := '0'B,
      connect_return_code := 0
    }
  }
  
  template MQTT_v3_1_1_ReqResp t_disconnect := 
  {
    disconnect_msg := {
      header := { flags := '0000'B }
    }
  }
  
  function f_handleVerdict(in boolean p_verdict)
  {
    if (p_verdict) { setverdict(pass) }
    else { setverdict(fail) }
  }
  
  function f_nextPacketId(inout integer p_id)
  {
    p_id := p_id + 1;
  }
  
  control
  {
    execute(tc_Mqtt_client_connect_disconnect());
    execute(tc_Mqtt_client_subscribe());
    execute(tc_Mqtt_client_unsubscribe());
    execute(tc_Mqtt_client_publish_qos0());
    execute(tc_Mqtt_client_publish_qos1());
  }
}
